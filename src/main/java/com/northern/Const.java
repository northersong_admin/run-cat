package com.northern;

import javax.swing.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public interface Const {

    String sleepCat = "/icon/cat_sleep.png";

    String runCat = "/icon/cat_%d.png";

    List<ImageIcon> runIcons = IntStream.range(0, 5).boxed()
            .map(i -> {
                ImageIcon icon = new ImageIcon(Const.class.getResource(String.format(Const.runCat, i)));
                icon.setDescription(String.format(Const.runCat, i));
                return icon;
            }).collect(Collectors.toList());
}
