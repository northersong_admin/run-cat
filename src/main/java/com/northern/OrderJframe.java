package com.northern;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.function.Function;

public class OrderJframe extends JFrame {

    public OrderJframe() throws HeadlessException {
        setTitle("orderDetail");

        JTextField field = new JTextField();
        field.setName("orderId");

        JPanel jp = new JPanel();
        GridLayout gLayout = new GridLayout();
        gLayout.setColumns(2);
        gLayout.setRows(3);
        jp.setLayout(gLayout);

        Container container = getContentPane();
        container.add(jp);

        // 第一行
        jp.add(field);
        jp.add(new JLabel("订单号"));

        // 第二行
        jp.add(browseButton(field, "clog", this::tagOrderId));
        jp.add(browseButton(field, "offline-prod", orderId->goOffline(true, orderId)));

        // 第三行
        jp.add(browseButton(field, "offline-fat47", orderId->goOffline(false, orderId)));
        jp.add(morning(field));
    }

    public String goOffline(boolean isProd, String orderId) {
        return isProd ?
                "http://order.car.ctripcorp.com/sdo/customerservice/dist/index.html?module=111133#/orderInfo/info?orderId=" + orderId
                : "http://order.car.fat47.qa.nt.ctripcorp.com/sdo/customerservice/dist/index.html#/orderInfo/info?orderId=" + orderId;
    }

    public String tagOrderId(String orderId) {
        return "http://logcube.ctripcorp.com/#/?app=100024690~visibility=0,1~tags=orderId%3D" + orderId;
    }


    public JButton browseButton(JTextField field, String buttonName, Function<String, String> url) {
        JButton button = new JButton();
        button.setText(buttonName);
        JFrame jFrame = this;
        button.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    Desktop.getDesktop().browse(
                            new java.net.URI(url.apply(field.getText())));
                    jFrame.setVisible(false);
                } catch (IOException | URISyntaxException ex) {
                    ex.printStackTrace();
                }
            }
        });
        return button;
    }

    public JButton morning(JTextField field) {
        JButton button = new JButton();
        button.setText("早上来了要看的");
        JFrame jFrame = this;
        button.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    // 注意上面是连续打开的，而macOS在使用分屏功能的时候，可能找不到最后一个窗口在哪里....
                    // 订详性能
                    Desktop.getDesktop().browse(new java.net.URI("http://nova.ops.ctripcorp.com/#/dashboard/ea62815e-186b-4aa8-9071-be99850cf0fc"));
                    // 报警
                    Desktop.getDesktop().browse(new java.net.URI("http://bigeyes.ctripcorp.com/#/alert/InstanceList"));
                    // 邮件
                    Desktop.getDesktop().open(new File("/Applications/Microsoft Outlook.app"));
                    jFrame.setVisible(false);
                } catch (IOException | URISyntaxException ex) {
                    ex.printStackTrace();
                }
            }
        });
        return button;
    }
}
