package com.northern;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class CatTrayIcon {

    public static ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    private final TrayIcon trayIcon;
    private final ImageIcon icon;

    public static int index = 0;

    public static boolean isRun = false;
    public static ScheduledFuture<?> future;


    public CatTrayIcon(JFrame jFrame) {
        icon = new ImageIcon(Const.class.getResource(Const.sleepCat));
        jFrame.setIconImage(icon.getImage());

        trayIcon = new TrayIcon(icon.getImage());
        trayIcon.addMouseListener(new MouseAdapter() {
            // 鼠标事件
            public void mouseClicked(MouseEvent e) {
                // 点3下就退出
                if (e.getClickCount() == 3) {
                    System.exit(1);
                    return;
                }

                if (e.getButton() == MouseEvent.BUTTON3) {
                    jFrame.setLocation(e.getLocationOnScreen());
                    jFrame.setAlwaysOnTop(true);
                    jFrame.pack();
                    jFrame.setVisible(true);
                    return;
                }

                if (isRun && future != null) {
                    future.cancel(true);
                    trayIcon.setImage(icon.getImage());
                    isRun = false;
                } else {
                    future = executorService.scheduleAtFixedRate(() -> run(trayIcon), 0, 300, TimeUnit.MILLISECONDS);
                    isRun = true;
                }
            }
        });
    }

    public TrayIcon getTrayIcon() {
        return trayIcon;
    }

    public ImageIcon getIcon() {
        return icon;
    }

    public void run(TrayIcon trayIcon) {
        trayIcon.setImage(Const.runIcons.get(index).getImage());
        index++;
        if (index > 4) {
            index = 0;
        }
    }

}
