package com.northern;

import javax.swing.*;
import java.awt.*;

public class Main {

    public static void main(String[] args) throws AWTException {
        if (!SystemTray.isSupported()) {
            System.out.println("no support");
        }

        // 窗体
        JFrame jFrame = new OrderJframe();

        CatTrayIcon catTrayIcon = new CatTrayIcon(jFrame);
        jFrame.setIconImage(catTrayIcon.getIcon().getImage());
        SystemTray.getSystemTray().add(catTrayIcon.getTrayIcon());
    }
}
