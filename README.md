# runCat

## 介绍

一只猫在任务栏跑啊跑，点击后睡觉，再点继续跑，点3次退出

灵感来自 https://github.com/Kyome22/RunCat_for_windows
图片资源也来于此，感谢    
![Demo](doc/demo.gif)

### 2021年11月23日
新增功能:  
右键点击后，弹窗，输入订单号，可以直接日志，进入offline订单详情   
![img.png](doc/img.png)



#### 安装教程

mac m1的可以直接下载我打包的版本

其他端，可以使用 `jpackage` 打包
```shell
#macOs 示意
# 需要先执行mvn clean package 打出jar包，放在单独目录下。执行
jpackage --input ./  --main-jar runCat-1.0-SNAPSHOT-jar-with-dependencies.jar --verbose --type dmg  --name runCat
```
- input 放jar的目录
- main-jar jar包名
- type dmg mac平台，win的可以换exe试试。我没测试
- name 打出来的app名 

#### 使用说明
mac打出来后，像平常应用一样执行即可


#### 其他
原本想使用graalVM,也打包成功了，只有11M左右，但是他在macOS上不支持awt！在其github上发现了还打开的issue，我就放弃了.. 
https://github.com/oracle/graal/issues/2842

因 `jpackage` 是jdk14出的，所以我用的jdk版本是`17`。如果使用graalVM，建议使用11，17有bug。上面的issue，只是macOs不支持awt..linux和windowsu是修复了的

> graalVM的入门到放弃 https://my.oschina.net/northerSong/blog/5321219